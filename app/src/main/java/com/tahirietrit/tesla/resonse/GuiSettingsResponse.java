package com.tahirietrit.tesla.resonse;

import com.tahirietrit.tesla.model.GuiSettings;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by etrit on 16-09-16.
 */
public class GuiSettingsResponse {

    @SerializedName("response")
    @Expose
    private GuiSettings response;

    public GuiSettings getGuiesttings() {
        return response;
    }

    public void setResponse(GuiSettings response) {
        this.response = response;
    }

}