package com.tahirietrit.tesla.resonse;

import com.tahirietrit.tesla.model.DrivePosition;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DrivePositionResponse  {

    @SerializedName("response")
    @Expose
    private DrivePosition response;

    public DrivePosition getDrivePosition() {
        return response;
    }

    public void setResponse(DrivePosition response) {
        this.response = response;
    }

}