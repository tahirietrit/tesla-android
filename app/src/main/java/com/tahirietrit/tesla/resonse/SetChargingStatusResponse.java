package com.tahirietrit.tesla.resonse;

import com.tahirietrit.tesla.model.SetChargingStatus;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class SetChargingStatusResponse {

    @SerializedName("response")
    @Expose
    private SetChargingStatus setChargingStatus;

    public SetChargingStatus getResponse() {
        return setChargingStatus;
    }

    public void setResponse(SetChargingStatus setChargingStatus) {
        this.setChargingStatus = setChargingStatus;
    }

    @Override
    public String toString() {
        return "\n\nSetChargingStatusResponse\n{" +
                "setChargingStatus=" + setChargingStatus.toString() +
                '}';
    }
}
