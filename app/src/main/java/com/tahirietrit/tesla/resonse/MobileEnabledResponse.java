package com.tahirietrit.tesla.resonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class MobileEnabledResponse {

    @SerializedName("response")
    @Expose
    private Boolean response;

    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }

}