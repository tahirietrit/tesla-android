package com.tahirietrit.tesla.resonse;

import com.tahirietrit.tesla.model.Vehicle;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macb on 16/08/16.
 */
public class VehiclesResponse {

    @SerializedName("response")
    @Expose
    private List<Vehicle> vehicles = new ArrayList<Vehicle>();
    @SerializedName("count")
    @Expose
    private Integer count;

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> response) {
        this.vehicles = response;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "VehiclesResponse\n{" +
                "count=" + count +
                ", vehicles=" + vehicles.get(0).toString() +
                '}';

    }
}