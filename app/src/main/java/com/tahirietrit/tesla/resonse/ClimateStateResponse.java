package com.tahirietrit.tesla.resonse;

import com.tahirietrit.tesla.model.ClimateState;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClimateStateResponse {

    @SerializedName("response")
    @Expose
    private ClimateState climateState;

    public ClimateState getClimateState() {
        return climateState;
    }

    public void setClimateState(ClimateState climateState) {
        this.climateState = climateState;
    }
}