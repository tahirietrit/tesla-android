package com.tahirietrit.tesla.resonse;

import com.tahirietrit.tesla.model.ChargingStatus;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class ChargingStatusResponse {

    @SerializedName("response")
    @Expose
    private ChargingStatus chargingStatus;

    public ChargingStatus getResponse() {
        return chargingStatus;
    }

    public void setResponse(ChargingStatus chargingStatus) {
        this.chargingStatus = chargingStatus;
    }

    @Override
    public String toString() {
        return "\n\nChargingStatusResponse\n{" +
                "chargingStatus=" + chargingStatus.toString() +
                '}';
    }
}