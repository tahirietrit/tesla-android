package com.tahirietrit.tesla.resonse;

import com.tahirietrit.tesla.model.VehicleState;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by etrit on 16-09-06.
 */
public class VehicleStateResponse {

    @SerializedName("response")
    @Expose
    private VehicleState vehicleState;

    public VehicleState getVehicleState() {
        return vehicleState;
    }

    public void setVehicleState(VehicleState vehicleState) {
        this.vehicleState = vehicleState;
    }

}