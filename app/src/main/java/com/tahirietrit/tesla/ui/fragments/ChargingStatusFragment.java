package com.tahirietrit.tesla.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.tahirietrit.tesla.R;
import com.tahirietrit.tesla.di.TeslaComponent;
import com.tahirietrit.tesla.model.ChargingStatus;
import com.tahirietrit.tesla.utils.ConvertUtilitys;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ChargingStatusFragment extends TeslaBaseFragment {

    public static final String STATUS_CHARGING = "charging";
    private static final String MSG_TAG = "msg";


    List<Integer> chargingLimits = new ArrayList<>();
    ArrayAdapter<Integer> percentageArrayAdapter;

    @BindView(R.id.charging_status)
    TextView chargingStatusTextView;
    @BindView(R.id.time_remaining)
    TextView timeRemainingTextView;
    @BindView(R.id.current_battery_level)
    TextView currentBatteryLvl;
    @BindView(R.id.charging_limit_spinner)
    Spinner chargingLimitSpinner;
    @BindView(R.id.set_charging_limit_button)
    Button setChargingLimitButton;
    @BindView(R.id.charging_rate_data)
    TextView chargingRateDataTextView;


    public static ChargingStatusFragment newInstance(String text) {

        ChargingStatusFragment f = new ChargingStatusFragment();
        Bundle b = new Bundle();
        b.putString(MSG_TAG, text);

        f.setArguments(b);

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.charging_status_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        for (int i = 50; i <= 100; i = i + 5) {
            chargingLimits.add(i);
        }
        percentageArrayAdapter = new ArrayAdapter<Integer>(getActivity(),
                R.layout.support_simple_spinner_dropdown_item, chargingLimits);
        chargingLimitSpinner.setAdapter(percentageArrayAdapter);
        getVehicleChargingStatus();

    }

    @OnClick(R.id.refresh_button)
    void getVehicleChargingStatus() {
        apiService.getChargingStatus(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(chargingStatusResponseResponse -> {
                    loggingView.addExcutionTimeToLog(chargingStatusResponseResponse);
                })
                .flatMap(chargingStatusResponseResponse -> Observable.just(chargingStatusResponseResponse.body()))
                .repeatWhen(completed -> completed.delay(10, TimeUnit.MINUTES))
                .doOnNext(chargingStatusResponse -> loggingView.addNewReponseToLog("Charging Status Response:\n"
                        + gson.toJson(chargingStatusResponse)))
                .subscribe(chargingStatusResponse -> {
                    updateUi(chargingStatusResponse.getResponse());
                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

    @OnClick(R.id.set_charging_limit_button)
    void setChargingLimit() {
        apiService.setChargingLimit(appPreferences.getAccessToken(), appPreferences.getVehicleId(),
                (Integer) chargingLimitSpinner.getSelectedItem())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(setChargingStatusResponseResponse -> {
                    loggingView.addExcutionTimeToLog(setChargingStatusResponseResponse);
                })
                .flatMap(setChargingStatusResponseResponse -> Observable.just(setChargingStatusResponseResponse.body()))
                .subscribe(setChargingStatusResponse -> {
                    loggingView.addNewReponseToLog("Set Charging Limit Response:\n" + gson.toJson(setChargingStatusResponse));
                });
    }

    @OnClick(R.id.share_button)
    void shareLog() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, loggingView.getLog());
        shareIntent.setType("text/plain");
        startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.share_dialog_tittle)));
    }

    private void updateUi(ChargingStatus chargingStatus) {
        int currentChargingLimit = chargingStatus.getChargeLimitSoc();
        if (chargingLimits.contains(currentChargingLimit)) {
            chargingLimitSpinner.setSelection(chargingLimits.indexOf(currentChargingLimit));
        } else {
            for (int i = 0; i < chargingLimits.size() - 1; i++) {
                float dI = chargingLimits.get(i);
                if (currentChargingLimit > dI && currentChargingLimit < chargingLimits.get(i + 1)) {
                    chargingLimits.add(i + 1, currentChargingLimit);
                    chargingLimitSpinner.setSelection(i + 1);
                    percentageArrayAdapter.notifyDataSetChanged();
                }
            }
        }
        chargingStatusTextView.setText(chargingStatus.getChargingState());
        currentBatteryLvl.setText(chargingStatus.getBatteryLevel() + " %");
        if (STATUS_CHARGING.equalsIgnoreCase(chargingStatus.getChargingState())) {
            timeRemainingTextView.setVisibility(View.VISIBLE);
            chargingRateDataTextView.setVisibility(View.VISIBLE);
            timeRemainingTextView.setText(ConvertUtilitys.getRemeaningTime(chargingStatus.getTimeToFullCharge()) + " remaining");
            chargingRateDataTextView.setText(getChargingRate(chargingStatus));
        } else {
            timeRemainingTextView.setVisibility(View.GONE);
            chargingRateDataTextView.setVisibility(View.GONE);
        }
    }

    private String getChargingRate(ChargingStatus chargingStatus) {
        return ConvertUtilitys.voidConvertMiToKm(chargingStatus.getChargeRate()) + " km/hr "
                + chargingStatus.getChargerVoltage() + " V "
                + chargingStatus.getChargerActualCurrent() + "/"
                + chargingStatus.getChargerPilotCurrent() + " A";
    }

    @Override
    protected void inject(TeslaComponent component) {
        component.inject(this);
    }


}
