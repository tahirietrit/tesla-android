package com.tahirietrit.tesla.ui.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.tahirietrit.tesla.TeslaApplication;
import com.tahirietrit.tesla.api.ApiService;
import com.tahirietrit.tesla.di.TeslaComponent;
import com.tahirietrit.tesla.eventbus.RxBus;
import com.tahirietrit.tesla.persistency.AppPreferences;
import com.tahirietrit.tesla.utils.LoggingView;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Named;

public abstract class TeslaBaseFragment extends Fragment {

    @Inject
    @Named("withRxBus")
    ApiService apiService;
    @Inject
    AppPreferences appPreferences;
    @Inject
    RxBus rxBus;
    @Inject
    LoggingView loggingView;
    @Inject
    Gson gson;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject(((TeslaApplication) getActivity().getApplication()).getComponent());
    }

    protected abstract void inject(TeslaComponent component);
}
