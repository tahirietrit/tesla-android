package com.tahirietrit.tesla.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.tahirietrit.tesla.R;
import com.tahirietrit.tesla.di.TeslaComponent;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class ApiExploreFragment extends TeslaBaseFragment {

    private static final String MSG_TAG = "msg";

    String[] callsList;

    @BindView(R.id.calls_list_view)
    ListView callsListView;


    public static ApiExploreFragment newInstance(String text) {

        ApiExploreFragment f = new ApiExploreFragment();
        Bundle b = new Bundle();
        b.putString(MSG_TAG, text);

        f.setArguments(b);

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.api_explore_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        callsList = getActivity().getResources().getStringArray(R.array.calls);
        callsListView.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, callsList));
        callsListView.setOnItemClickListener((adapterView, view1, i, l) -> makeTheCallBasedOnClickedItem(i));
    }

    @Override
    protected void inject(TeslaComponent component) {
        component.inject(this);
    }

    private void makeTheCallBasedOnClickedItem(int position) {
        switch (position) {
            case 0:
                getMobileAccess();
                break;
            case 1:
                getChargeState();
                break;
            case 2:
                getClimateSettings();
                break;
            case 3:
                getDrivingPosition();
                break;
            case 4:
                getGuiSettings();
                break;
            case 5:
                getVehicleState();
                break;
        }
    }

    private void getMobileAccess() {
        apiService.getMobileEnabled(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(mobileEnabledResponseResponse -> {
                    loggingView.addExcutionTimeToLog(mobileEnabledResponseResponse);
                })
                .flatMap(mobileEnabledResponseResponse -> Observable.just(mobileEnabledResponseResponse.body()))
                .subscribe(mobileEnabledResponse -> {
                    loggingView.addNewReponseToLog("Mobile Enabled Response:\n" + gson.toJson(mobileEnabledResponse));
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void getChargeState() {
        apiService.getChargingStatus(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(chargingStatusResponseResponse -> {
                    loggingView.addExcutionTimeToLog(chargingStatusResponseResponse);
                })
                .flatMap(chargingStatusResponseResponse -> Observable.just(chargingStatusResponseResponse.body()))
                .repeatWhen(completed -> completed.delay(10, TimeUnit.MINUTES))
                .doOnNext(chargingStatusResponse -> loggingView.addNewReponseToLog("Charging Status Response:\n"
                        + gson.toJson(chargingStatusResponse)))
                .subscribe(chargingStatusResponse -> {
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void getClimateSettings() {
        apiService.getClimateState(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(climateStateResponseResponse -> {
                    loggingView.addExcutionTimeToLog(climateStateResponseResponse);
                })
                .flatMap(climateStateResponseResponse -> Observable.just(climateStateResponseResponse.body()))
                .subscribe(climateStateResponse -> {
                    loggingView.addNewReponseToLog("Climate State Response:\n" + gson.toJson(climateStateResponse));
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void getDrivingPosition() {
        apiService.getDrivingPosition(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(drivePositionResponseResponse -> {
                    loggingView.addExcutionTimeToLog(drivePositionResponseResponse);
                })
                .flatMap(drivePositionResponseResponse -> Observable.just(drivePositionResponseResponse.body()))
                .subscribe(drivePositionResponse -> {
                    loggingView.addNewReponseToLog("Driving Position Response:\n" + gson.toJson(drivePositionResponse));
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void getGuiSettings() {
        apiService.getGuiSettings(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(guiSettingsResponseResponse -> {
                    loggingView.addExcutionTimeToLog(guiSettingsResponseResponse);
                })
                .flatMap(guiSettingsResponseResponse -> Observable.just(guiSettingsResponseResponse.body()))
                .subscribe(guiSettingsResponse -> {
                    loggingView.addNewReponseToLog("GUI Settings Response:\n" + gson.toJson(guiSettingsResponse));
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void getVehicleState() {
        apiService.getVehicleState(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(vehicleStateResponseResponse -> {
                    loggingView.addExcutionTimeToLog(vehicleStateResponseResponse);
                })
                .flatMap(vehicleStateResponseResponse -> Observable.just(vehicleStateResponseResponse.body()))
                .subscribe(vehicleStateResponse -> {
                    loggingView.addNewReponseToLog("Vehicle State Response:\n" + gson.toJson(vehicleStateResponse));
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }
}
