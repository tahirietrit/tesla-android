package com.tahirietrit.tesla.ui.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tahirietrit.tesla.R;
import com.tahirietrit.tesla.di.TeslaComponent;
import com.tahirietrit.tesla.model.ClimateState;
import com.tahirietrit.tesla.model.VehicleState;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RoofAcFragment extends TeslaBaseFragment {

    private static final String MSG_TAG = "msg";

    private static final String NO_ROOF_DOOR = "No sun roof";
    private static final String CLOSED = "Closed";
    private static final String STATE_OPEN = "open";
    private static final String STATE_VENT = "vent";
    private static final String STATE_CLOSE = "close";

    boolean isAcOn;
    private double driverTempSeting;

    @BindView(R.id.open_button)
    TextView openButton;
    @BindView(R.id.vent_button)
    TextView ventButton;
    @BindView(R.id.close_button)
    TextView closeButton;
    @BindView(R.id.user_set_temp_textview)
    TextView userSetTemp;
    @BindView(R.id.roof_state_textview)
    TextView roofStateTextView;

    @BindView(R.id.ac_background)
    RelativeLayout acBackground;


    public static RoofAcFragment newInstance(String text) {

        RoofAcFragment f = new RoofAcFragment();
        Bundle b = new Bundle();
        b.putString(MSG_TAG, text);

        f.setArguments(b);

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.widget_func_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        getClimateState();
        getVehicleState();

        openButton.setOnClickListener(view1 -> controlRoof(STATE_OPEN));
        ventButton.setOnClickListener(view1 -> controlRoof(STATE_VENT));
        closeButton.setOnClickListener(view1 -> controlRoof(STATE_CLOSE));
    }

    @Override
    protected void inject(TeslaComponent component) {
        component.inject(this);
    }

    @OnClick(R.id.ac_background)
    void switchAc() {
        if (isAcOn) {
            turnAcOff();
        } else {
            turnAcOn();
        }
    }

    private void getClimateState() {
        apiService.getClimateState(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(climateStateResponseResponse -> {
                    loggingView.addExcutionTimeToLog(climateStateResponseResponse);
                })
                .flatMap(climateStateResponseResponse -> Observable.just(climateStateResponseResponse.body()))
                .subscribe(climateStateResponse -> {
                    updateAcUI(climateStateResponse.getClimateState());
                    loggingView.addNewReponseToLog("Climate State Response:\n" + gson.toJson(climateStateResponse));
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void turnAcOn() {
        apiService.startAC(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(setChargingStatusResponseResponse -> {
                    loggingView.addExcutionTimeToLog(setChargingStatusResponseResponse);
                })
                .flatMap(setChargingStatusResponseResponse -> Observable.just(setChargingStatusResponseResponse.body()))
                .subscribe(setChargingStatusResponse -> {
                    loggingView.addNewReponseToLog("Turn AC On Response:\n" + gson.toJson(setChargingStatusResponse));
                    updateAcUI(new ClimateState(driverTempSeting, 1));
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void turnAcOff() {
        apiService.stopAC(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(setChargingStatusResponseResponse -> {
                    loggingView.addExcutionTimeToLog(setChargingStatusResponseResponse);
                })
                .flatMap(setChargingStatusResponseResponse -> Observable.just(setChargingStatusResponseResponse.body()))
                .subscribe(setChargingStatusResponse -> {
                    loggingView.addNewReponseToLog("Turn AC Off Response:\n" + gson.toJson(setChargingStatusResponse));
                    updateAcUI(new ClimateState(driverTempSeting, 1));
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void getVehicleState() {
        apiService.getVehicleState(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(vehicleStateResponseResponse -> {
                    loggingView.addExcutionTimeToLog(vehicleStateResponseResponse);
                })
                .flatMap(vehicleStateResponseResponse -> Observable.just(vehicleStateResponseResponse.body()))
                .subscribe(vehicleStateResponse -> {
                    updateRoofUI(vehicleStateResponse.getVehicleState());
                    loggingView.addNewReponseToLog("Vehicle State Response:\n" + gson.toJson(vehicleStateResponse));
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void controlRoof(String state) {
        apiService.controlTheRoof(appPreferences.getAccessToken(), appPreferences.getVehicleId(), state)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(vehicleStateResponseResponse -> {
                    loggingView.addExcutionTimeToLog(vehicleStateResponseResponse);
                })
                .flatMap(vehicleStateResponseResponse -> Observable.just(vehicleStateResponseResponse.body()))
                .subscribe(vehicleStateResponse -> {
                    getVehicleState();
                    loggingView.addNewReponseToLog("Move Roof Response:\n" + gson.toJson(vehicleStateResponse));
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void updateAcUI(ClimateState climateState) {

        isAcOn = climateState.getFanStatus() != null && climateState.getFanStatus() > 0;

        if (isAcOn) {
            acBackground.setBackgroundColor(Color.GREEN);
        } else {
            acBackground.setBackgroundColor(Color.RED);
        }
        driverTempSeting = climateState.getDriverTempSetting();
        userSetTemp.setText(climateState.getDriverTempSetting() + "°C");
    }

    private void updateRoofUI(VehicleState vehicleState) {
        if (vehicleState.getSunRoofInstalled() != null) {
            if (vehicleState.getSunRoofPercentOpen() == 0) {
                roofStateTextView.setText(CLOSED);
            } else {
                roofStateTextView.setText(vehicleState.getSunRoofState());
            }
        } else {
            roofStateTextView.setText(NO_ROOF_DOOR);
        }
    }

}
