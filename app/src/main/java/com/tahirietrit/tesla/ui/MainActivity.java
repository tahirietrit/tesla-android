package com.tahirietrit.tesla.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.tahirietrit.tesla.R;
import com.tahirietrit.tesla.TeslaApplication;
import com.tahirietrit.tesla.api.ApiService;
import com.tahirietrit.tesla.eventbus.RxBus;
import com.tahirietrit.tesla.persistency.AppPreferences;
import com.tahirietrit.tesla.ui.adapters.TeslaAppPagerAdapter;
import com.tahirietrit.tesla.utils.LoggingView;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Request;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    public static final int UNAUTHORIZED_CODE = 401;
    public static final int POSITION_CHARGING_STATUS = 0;
    public static final int POSITION_AC_ROOF = 1;
    public static final int POSITION_EXPLORER = 2;

    @BindView(R.id.logging_textivew)
    TextView loggingTextView;

    @BindView(R.id.tesla_pager)
    ViewPager teslaPager;

    @Inject
    @Named("withRxBus")
    ApiService apiService;
    @Inject
    AppPreferences appPreferences;
    @Inject
    RxBus rxBus;
    @Inject
    LoggingView loggingView;
    @Inject
    Gson gson;

    TeslaAppPagerAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ((TeslaApplication) getApplication()).getComponent().inject(this);
        loggingView.setLogingTextView(loggingTextView);
        adapter = new TeslaAppPagerAdapter(getSupportFragmentManager());

        getVehicleList();

        rxBus.toObserverable()
                .observeOn(AndroidSchedulers.mainThread())
                .map(o -> {
                    if (o instanceof Request) {
                        Request request = (Request) o;
                        return request.method() + "\n" + request.url() + "\n" + request.headers();
                    } else {
                        throw new IllegalArgumentException("Unexpected type in RxBus:" + o.getClass());
                    }
                })
                .subscribe(requestData -> {
                    loggingView.addNewRequestdataToLog(requestData);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void getVehicleList() {
        apiService.getVehicles(appPreferences.getAccessToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(vehiclesResponseResponse -> {
                    if (vehiclesResponseResponse.code() == UNAUTHORIZED_CODE) {
                        logoutUser();
                    } else {

                        loggingView.addExcutionTimeToLog(vehiclesResponseResponse);
                    }
                })
                .flatMap(vehiclesResponseResponse -> Observable.just(vehiclesResponseResponse.body()))
                .doOnNext(vehiclesResponse -> appPreferences.saveVehicleId(vehiclesResponse.getVehicles().get(0).getId()))
                .subscribe(vehiclesResponse -> {
                    loggingView.addNewReponseToLog(gson.toJson(vehiclesResponse));
                    teslaPager.setAdapter(adapter);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    @OnClick(R.id.charging_status_button)
    void showChargingStatus() {
        teslaPager.setCurrentItem(POSITION_CHARGING_STATUS);
    }

    @OnClick(R.id.widget_func_button)
    void showWidgetFunc() {
        teslaPager.setCurrentItem(POSITION_AC_ROOF);
    }

    @OnClick(R.id.api_explore_button)
    void showExplorer() {
        teslaPager.setCurrentItem(POSITION_EXPLORER);
    }

    private void logoutUser() {
        appPreferences.deleteExpiredAccessToken();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(LoginActivity.EXTRA_EMAIL, appPreferences.getUserEmail());
        startActivity(intent);
        finish();
    }


}
