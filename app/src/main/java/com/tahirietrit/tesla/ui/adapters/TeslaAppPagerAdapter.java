package com.tahirietrit.tesla.ui.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tahirietrit.tesla.ui.fragments.ApiExploreFragment;
import com.tahirietrit.tesla.ui.fragments.ChargingStatusFragment;
import com.tahirietrit.tesla.ui.fragments.RoofAcFragment;

public class TeslaAppPagerAdapter extends FragmentPagerAdapter {

    public TeslaAppPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int pos) {
        switch (pos) {
            case 0:
                return ChargingStatusFragment.newInstance("ChargingStatusFragment");
            case 1:
                return RoofAcFragment.newInstance("RoofAcFragment");
            case 2:
                return ApiExploreFragment.newInstance("ApiExploreFragment");
            default:
                throw new RuntimeException("Unsupported position");
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
