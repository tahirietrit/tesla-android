package com.tahirietrit.tesla.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tahirietrit.tesla.R;
import com.tahirietrit.tesla.TeslaApplication;
import com.tahirietrit.tesla.api.ApiService;
import com.tahirietrit.tesla.persistency.AppPreferences;
import com.tahirietrit.tesla.utils.ApiUtils;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by macb on 12/08/16.
 */
public class LoginActivity extends Activity {

    public static final String EXTRA_EMAIL = "email";

    @BindView(R.id.email_edittext)
    EditText emailEditText;
    @BindView(R.id.password_edittext)
    EditText passwordEditText;
    @BindView(R.id.login_button)
    Button loginButton;

    @Inject
    @Named("withRxBus")
    ApiService apiService;
    @Inject
    AppPreferences appPreferences;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen_layout);
        ButterKnife.bind(this);
        ((TeslaApplication) getApplication()).getComponent().inject(this);

        if (appPreferences.getAccessToken() != null) {
            startMainActivity();
        }

        String storedEmail = getIntent().getStringExtra(EXTRA_EMAIL);
        emailEditText.setText(storedEmail);
    }

    @OnClick(R.id.login_button)
    void doLogin() {
        if (isValidEmail(getEmail()) && getPassword().length() > 0) {
            apiService.getToken(ApiUtils.GRANT_TYPE,
                    ApiUtils.CLIENT_ID,
                    ApiUtils.CLIENT_SECRET,
                    getEmail(),
                    getPassword())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(token -> {
                        appPreferences.saveAccessToken("Bearer " + token.getAccessToken());
                        appPreferences.saveUserEmail(getEmail());
                    })
                    .subscribe(token -> {
                        if (token != null) {
                            startMainActivity();
                        }
                    }, throwable -> {
                        throwable.printStackTrace();
                    });

        } else {
            Toast.makeText(getApplicationContext(), "Please enter email and passord", Toast.LENGTH_SHORT).show();
        }
    }

    private String getEmail() {
        return emailEditText.getText().toString();
    }

    private String getPassword() {
        return passwordEditText.getText().toString();
    }

    private void startMainActivity() {
        Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(mainActivityIntent);
    }

    public static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
