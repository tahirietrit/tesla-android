package com.tahirietrit.tesla.persistency;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by macb on 15/08/16.
 */
public class AppPreferences {
    private SharedPreferences userPreferences;
    private static final String USER_PREFERENCES_KEY = "userPreferences";
    private static final String ACCESS_TOKEN_KEY = "accessToken";
    private static final String USER_EMAIL_KEY = "userEmail";
    private static final String VEHICLE_ID_KEY = "vehicleId";


    public AppPreferences(Context ctx) {
        userPreferences = ctx.getSharedPreferences(USER_PREFERENCES_KEY, Context.MODE_PRIVATE);
    }

    public void saveUserEmail(String userEmail) {
        userPreferences.edit().putString(USER_EMAIL_KEY, userEmail).commit();
    }

    public String getUserEmail() {
        return userPreferences.getString(USER_EMAIL_KEY, null);
    }

    public void saveAccessToken(String accessToken) {
        userPreferences.edit().putString(ACCESS_TOKEN_KEY, accessToken).commit();
    }

    public String getAccessToken() {
        return userPreferences.getString(ACCESS_TOKEN_KEY, null);
    }

    public void saveVehicleId(long vehicleId){
        userPreferences.edit().putLong(VEHICLE_ID_KEY, vehicleId).commit();
    }
    public long getVehicleId(){
        return userPreferences.getLong(VEHICLE_ID_KEY, 0);
    }

    public void deleteExpiredAccessToken(){
        userPreferences.edit().remove(ACCESS_TOKEN_KEY).commit();
    }

}
