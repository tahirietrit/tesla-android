package com.tahirietrit.tesla.widget.chargingstate;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RemoteViews;

import com.tahirietrit.tesla.R;
import com.tahirietrit.tesla.TeslaApplication;
import com.tahirietrit.tesla.api.ApiService;
import com.tahirietrit.tesla.model.ChargingStatus;
import com.tahirietrit.tesla.persistency.AppPreferences;
import com.tahirietrit.tesla.ui.fragments.ChargingStatusFragment;
import com.tahirietrit.tesla.utils.ConvertUtilitys;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ChargingStatusWidgetService extends Service {
    @Inject
    @Named("withoutRxBus")
    ApiService apiService;
    @Inject
    AppPreferences appPreferences;

    AppWidgetManager appWidgetProvider;
    RemoteViews remoteViews;
    int[] allWidgetIds;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            appWidgetProvider = AppWidgetManager.getInstance(this.getApplicationContext());
            allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);
            remoteViews = new RemoteViews(this.getApplicationContext().getPackageName(), R.layout.widget_layout);
            getVehicleChargingStatus();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((TeslaApplication) getApplication()).getComponent().inject(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void getVehicleChargingStatus() {
        apiService.getChargingStatus(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(chargingStatusResponseResponse -> Observable.just(chargingStatusResponseResponse.body()))
                .subscribe(chargingStatusResponse -> {
                    updateWidgetData(chargingStatusResponse.getResponse());
                    stopSelf();
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void updateWidgetData(ChargingStatus chargingStatus) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this
                .getApplicationContext());

        for (int widgetId : allWidgetIds) {

            remoteViews.setTextViewText(R.id.ideal_range_widget_textview, ConvertUtilitys.voidConvertMiToKm(chargingStatus.getIdealBatteryRange()) + " km");
            if (chargingStatus.getChargingState().equalsIgnoreCase(ChargingStatusFragment.STATUS_CHARGING)) {
                remoteViews.setTextViewText(R.id.charging_rate_widget_textview, getChargingRate(chargingStatus));
                remoteViews.setTextViewText(R.id.time_remaining_widget_textview, ConvertUtilitys.getRemeaningTime(chargingStatus.getTimeToFullCharge()));
            } else {
                remoteViews.setViewVisibility(R.id.charging_rate_widget_textview, View.GONE);
                remoteViews.setViewVisibility(R.id.time_remaining_widget_textview, View.GONE);
            }
            Intent clickIntent = new Intent(this.getApplicationContext(),
                    ChargingStatusWidgetProvider.class);

            clickIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,
                    allWidgetIds);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    getApplicationContext(), 0, clickIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.ideal_range_widget_textview, pendingIntent);
            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
    }

    private String getChargingRate(ChargingStatus chargingStatus) {
        return ConvertUtilitys.voidConvertMiToKm(chargingStatus.getChargeRate()) + " km/hr "
                + chargingStatus.getChargerVoltage() + " V "
                + chargingStatus.getChargerActualCurrent() + "/"
                + chargingStatus.getChargerPilotCurrent() + " A";
    }
}
