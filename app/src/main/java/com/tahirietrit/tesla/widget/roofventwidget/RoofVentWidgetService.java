package com.tahirietrit.tesla.widget.roofventwidget;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.RemoteViews;

import com.tahirietrit.tesla.R;
import com.tahirietrit.tesla.TeslaApplication;
import com.tahirietrit.tesla.api.ApiService;
import com.tahirietrit.tesla.model.VehicleState;
import com.tahirietrit.tesla.persistency.AppPreferences;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RoofVentWidgetService extends Service {
    @Inject
    @Named("withoutRxBus")
    ApiService apiService;
    @Inject
    AppPreferences appPreferences;

    AppWidgetManager appWidgetProvider;
    RemoteViews remoteViews;
    int[] allWidgetIds;

    private static final String NO_ROOF_DOOR = "No sun roof";
    private static final String CLOSED = "Closed";
    private static final String VENT = "vent";
    private static final String CLOSE = "close";
    private static final String CLICKED_KEY = "widgetClicked";
    private static final String ROOF_STATE = "roofState";


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            appWidgetProvider = AppWidgetManager.getInstance(this.getApplicationContext());
            allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);
            remoteViews = new RemoteViews(this.getApplicationContext().getPackageName(), R.layout.vent_roof_widget_layout);
            if (intent.getBooleanExtra(CLICKED_KEY, false)) {
                if (intent.getBooleanExtra(ROOF_STATE, false)) {
                    controlTheRoof(VENT);
                } else {
                    controlTheRoof(CLOSE);
                }
            } else {
                getVehicleState();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((TeslaApplication) getApplication()).getComponent().inject(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void getVehicleState() {
        apiService.getVehicleState(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(vehicleStateResponseResponse -> Observable.just(vehicleStateResponseResponse.body()))
                .subscribe(vehicleStateResponse -> {
                    updateWidgetData(vehicleStateResponse.getVehicleState());
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void updateWidgetData(VehicleState vehicleState) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getApplicationContext());
        boolean isRoofVented = false;
        for (int widgetId : allWidgetIds) {
            if (vehicleState.getSunRoofInstalled() != null) {
                if (vehicleState.getSunRoofPercentOpen() == 0) {
                    remoteViews.setTextViewText(R.id.roof_door_state, CLOSED);
                    isRoofVented = true;
                } else {
                    remoteViews.setTextViewText(R.id.roof_door_state, vehicleState.getSunRoofState());
                    isRoofVented = false;
                }
            } else {
                remoteViews.setTextViewText(R.id.roof_door_state, NO_ROOF_DOOR);
            }

            Intent clickIntent = new Intent(this.getApplicationContext(), RoofVentWidgetService.class);
            clickIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);
            clickIntent.putExtra(CLICKED_KEY, true);
            clickIntent.putExtra(ROOF_STATE, isRoofVented);

            PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0, clickIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.roof_door_state, pendingIntent);
            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
    }

    private void controlTheRoof(String state) {
        apiService.controlTheRoof(appPreferences.getAccessToken(), appPreferences.getVehicleId(), state)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(vehicleStateResponseResponse -> Observable.just(vehicleStateResponseResponse.body()))
                .subscribe(vehicleStateResponse -> {
                    getVehicleState();
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }
}
