package com.tahirietrit.tesla.widget.chargingstate;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

public class ChargingStatusReciver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        ComponentName thisWidget = new ComponentName(context,
                ChargingStatusWidgetProvider.class);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context.getApplicationContext());
        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        Intent i = new Intent(context.getApplicationContext(), ChargingStatusWidgetService.class);
        i.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);
        context.startService(i);
    }

}
