package com.tahirietrit.tesla.widget.aircondition;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.RemoteViews;

import com.tahirietrit.tesla.R;
import com.tahirietrit.tesla.TeslaApplication;
import com.tahirietrit.tesla.api.ApiService;
import com.tahirietrit.tesla.model.ClimateState;
import com.tahirietrit.tesla.persistency.AppPreferences;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class AirConditionWidgetService extends Service {
    @Inject
    @Named("withoutRxBus")
    ApiService apiService;
    @Inject
    AppPreferences appPreferences;

    AppWidgetManager appWidgetProvider;
    RemoteViews remoteViews;
    int[] allWidgetIds;

    private static final String CLICKED_KEY = "widgetClicked";
    private static final String AC_STATE = "acState";
    private double driverTempSeting;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            appWidgetProvider = AppWidgetManager.getInstance(this.getApplicationContext());
            allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);
            remoteViews = new RemoteViews(this.getApplicationContext().getPackageName(), R.layout.air_condition_widget_layout);

            if (intent.getBooleanExtra(CLICKED_KEY, false)) {
                if (intent.getBooleanExtra(AC_STATE, false)) {
                    turnAcOff();
                } else {
                    turnAcOn();
                }

            } else {
                getClimateState();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((TeslaApplication) getApplication()).getComponent().inject(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void getClimateState() {
        apiService.getClimateState(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(climateStateResponseResponse -> Observable.just(climateStateResponseResponse.body()))
                .subscribe(climateStateResponse -> {
                    updateWidgetData(climateStateResponse.getClimateState());
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void turnAcOn() {
        apiService.startAC(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(setChargingStatusResponseResponse -> Observable.just(setChargingStatusResponseResponse.body()))
                .subscribe(setChargingStatusResponse -> {
                    updateWidgetData(new ClimateState(driverTempSeting, 1));
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void turnAcOff() {
        apiService.stopAC(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(setChargingStatusResponseResponse -> Observable.just(setChargingStatusResponseResponse.body()))
                .subscribe(setChargingStatusResponse -> {
                    getClimateState();
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void updateWidgetData(ClimateState climateState) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this
                .getApplicationContext());

        for (int widgetId : allWidgetIds) {
            driverTempSeting = climateState.getDriverTempSetting();
            remoteViews.setTextViewText(R.id.current_temp_level, driverTempSeting + "°C");
            boolean isAcOn;
            if (climateState.getFanStatus() != null) {
                isAcOn = climateState.getFanStatus() > 0;
            } else {
                isAcOn = false;
            }
            if (isAcOn) {
                remoteViews.setInt(R.id.widget_background, "setBackgroundColor", Color.GREEN);
            } else {
                remoteViews.setInt(R.id.widget_background, "setBackgroundColor", Color.RED);

            }
            Intent clickIntent = new Intent(this.getApplicationContext(), AirConditionWidgetService.class);
            clickIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);
            clickIntent.putExtra(CLICKED_KEY, true);
            clickIntent.putExtra(AC_STATE, isAcOn);

            PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0, clickIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.current_temp_level, pendingIntent);
            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }

    }
}