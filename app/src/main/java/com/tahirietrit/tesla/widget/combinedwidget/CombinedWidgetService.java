package com.tahirietrit.tesla.widget.combinedwidget;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.RemoteViews;

import com.tahirietrit.tesla.R;
import com.tahirietrit.tesla.TeslaApplication;
import com.tahirietrit.tesla.api.ApiService;
import com.tahirietrit.tesla.persistency.AppPreferences;

import javax.inject.Inject;
import javax.inject.Named;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CombinedWidgetService extends Service {
    @Inject
    @Named("withoutRxBus")
    ApiService apiService;
    @Inject
    AppPreferences appPreferences;

    AppWidgetManager appWidgetProvider;
    RemoteViews remoteViews;
    int[] allWidgetIds;

    private static final String CLICKED_KEY = "widgetClicked";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            appWidgetProvider = AppWidgetManager.getInstance(this.getApplicationContext());
            allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);
            remoteViews = new RemoteViews(this.getApplicationContext().getPackageName(), R.layout.combined_widget_layout);
            updateWidgetData();

            if (intent.getBooleanExtra(CLICKED_KEY, false)) {
                ventTheRoof();
                coolTheCar();

            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((TeslaApplication) getApplication()).getComponent().inject(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void coolTheCar() {
        apiService.startAC(appPreferences.getAccessToken(), appPreferences.getVehicleId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(setChargingStatusResponse -> {
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void ventTheRoof() {
        apiService.controlTheRoof(appPreferences.getAccessToken(), appPreferences.getVehicleId(), "vent")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(setChargingStatusResponse -> {
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void updateWidgetData() {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getApplicationContext());
        for (int widgetId : allWidgetIds) {
            Intent clickIntent = new Intent(this.getApplicationContext(), CombinedWidgetService.class);
            clickIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);
            clickIntent.putExtra(CLICKED_KEY, true);

            PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0, clickIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.cool_the_car, pendingIntent);
            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
    }
}