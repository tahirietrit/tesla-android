package com.tahirietrit.tesla.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GuiSettings {

    @SerializedName("gui_distance_units")
    @Expose
    private String guiDistanceUnits;
    @SerializedName("gui_temperature_units")
    @Expose
    private String guiTemperatureUnits;
    @SerializedName("gui_charge_rate_units")
    @Expose
    private String guiChargeRateUnits;
    @SerializedName("gui_24_hour_time")
    @Expose
    private Boolean gui24HourTime;
    @SerializedName("gui_range_display")
    @Expose
    private String guiRangeDisplay;

    public String getGuiDistanceUnits() {
        return guiDistanceUnits;
    }

    public void setGuiDistanceUnits(String guiDistanceUnits) {
        this.guiDistanceUnits = guiDistanceUnits;
    }

    public String getGuiTemperatureUnits() {
        return guiTemperatureUnits;
    }

    public void setGuiTemperatureUnits(String guiTemperatureUnits) {
        this.guiTemperatureUnits = guiTemperatureUnits;
    }

    public String getGuiChargeRateUnits() {
        return guiChargeRateUnits;
    }

    public void setGuiChargeRateUnits(String guiChargeRateUnits) {
        this.guiChargeRateUnits = guiChargeRateUnits;
    }

    public Boolean getGui24HourTime() {
        return gui24HourTime;
    }

    public void setGui24HourTime(Boolean gui24HourTime) {
        this.gui24HourTime = gui24HourTime;
    }

    public String getGuiRangeDisplay() {
        return guiRangeDisplay;
    }

    public void setGuiRangeDisplay(String guiRangeDisplay) {
        this.guiRangeDisplay = guiRangeDisplay;
    }
}