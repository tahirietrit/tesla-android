package com.tahirietrit.tesla.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClimateState {

    @SerializedName("inside_temp")
    @Expose
    private Object insideTemp;
    @SerializedName("outside_temp")
    @Expose
    private Object outsideTemp;
    @SerializedName("driver_temp_setting")
    @Expose
    private Double driverTempSetting;
    @SerializedName("passenger_temp_setting")
    @Expose
    private Double passengerTempSetting;
    @SerializedName("is_auto_conditioning_on")
    @Expose
    private Object isAutoConditioningOn;
    @SerializedName("is_front_defroster_on")
    @Expose
    private Object isFrontDefrosterOn;
    @SerializedName("is_rear_defroster_on")
    @Expose
    private Boolean isRearDefrosterOn;
    @SerializedName("fan_status")
    @Expose
    private Integer fanStatus;
    @SerializedName("seat_heater_left")
    @Expose
    private Integer seatHeaterLeft;
    @SerializedName("seat_heater_right")
    @Expose
    private Integer seatHeaterRight;
    @SerializedName("seat_heater_rear_left")
    @Expose
    private Integer seatHeaterRearLeft;
    @SerializedName("seat_heater_rear_right")
    @Expose
    private Integer seatHeaterRearRight;
    @SerializedName("seat_heater_rear_center")
    @Expose
    private Integer seatHeaterRearCenter;
    @SerializedName("seat_heater_rear_right_back")
    @Expose
    private Integer seatHeaterRearRightBack;
    @SerializedName("seat_heater_rear_left_back")
    @Expose
    private Integer seatHeaterRearLeftBack;
    @SerializedName("smart_preconditioning")
    @Expose
    private Boolean smartPreconditioning;

    public ClimateState(Double driverTempSetting, Integer fanStatus) {
        this.driverTempSetting = driverTempSetting;
        this.fanStatus = fanStatus;
    }

    public ClimateState() {
    }

    public Object getInsideTemp() {
        return insideTemp;
    }

    public void setInsideTemp(Object insideTemp) {
        this.insideTemp = insideTemp;
    }

    public Object getOutsideTemp() {
        return outsideTemp;
    }

    public void setOutsideTemp(Object outsideTemp) {
        this.outsideTemp = outsideTemp;
    }

    public Double getDriverTempSetting() {
        return driverTempSetting;
    }

    public void setDriverTempSetting(Double driverTempSetting) {
        this.driverTempSetting = driverTempSetting;
    }

    public Double getPassengerTempSetting() {
        return passengerTempSetting;
    }

    public void setPassengerTempSetting(Double passengerTempSetting) {
        this.passengerTempSetting = passengerTempSetting;
    }

    public Object getIsAutoConditioningOn() {
        return isAutoConditioningOn;
    }

    public void setIsAutoConditioningOn(Object isAutoConditioningOn) {
        this.isAutoConditioningOn = isAutoConditioningOn;
    }

    public Object getIsFrontDefrosterOn() {
        return isFrontDefrosterOn;
    }

    public void setIsFrontDefrosterOn(Object isFrontDefrosterOn) {
        this.isFrontDefrosterOn = isFrontDefrosterOn;
    }

    public Boolean getIsRearDefrosterOn() {
        return isRearDefrosterOn;
    }

    public void setIsRearDefrosterOn(Boolean isRearDefrosterOn) {
        this.isRearDefrosterOn = isRearDefrosterOn;
    }

    public Integer getFanStatus() {
        return fanStatus;
    }

    public void setFanStatus(Integer fanStatus) {
        this.fanStatus = fanStatus;
    }

    public Integer getSeatHeaterLeft() {
        return seatHeaterLeft;
    }

    public void setSeatHeaterLeft(Integer seatHeaterLeft) {
        this.seatHeaterLeft = seatHeaterLeft;
    }

    public Integer getSeatHeaterRight() {
        return seatHeaterRight;
    }

    public void setSeatHeaterRight(Integer seatHeaterRight) {
        this.seatHeaterRight = seatHeaterRight;
    }

    public Integer getSeatHeaterRearLeft() {
        return seatHeaterRearLeft;
    }

    public void setSeatHeaterRearLeft(Integer seatHeaterRearLeft) {
        this.seatHeaterRearLeft = seatHeaterRearLeft;
    }

    public Integer getSeatHeaterRearRight() {
        return seatHeaterRearRight;
    }

    public void setSeatHeaterRearRight(Integer seatHeaterRearRight) {
        this.seatHeaterRearRight = seatHeaterRearRight;
    }

    public Integer getSeatHeaterRearCenter() {
        return seatHeaterRearCenter;
    }

    public void setSeatHeaterRearCenter(Integer seatHeaterRearCenter) {
        this.seatHeaterRearCenter = seatHeaterRearCenter;
    }

    public Integer getSeatHeaterRearRightBack() {
        return seatHeaterRearRightBack;
    }

    public void setSeatHeaterRearRightBack(Integer seatHeaterRearRightBack) {
        this.seatHeaterRearRightBack = seatHeaterRearRightBack;
    }

    public Integer getSeatHeaterRearLeftBack() {
        return seatHeaterRearLeftBack;
    }

    public void setSeatHeaterRearLeftBack(Integer seatHeaterRearLeftBack) {
        this.seatHeaterRearLeftBack = seatHeaterRearLeftBack;
    }

    public Boolean getSmartPreconditioning() {
        return smartPreconditioning;
    }

    public void setSmartPreconditioning(Boolean smartPreconditioning) {
        this.smartPreconditioning = smartPreconditioning;
    }

}
