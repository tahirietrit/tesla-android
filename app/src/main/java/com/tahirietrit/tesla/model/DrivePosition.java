package com.tahirietrit.tesla.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DrivePosition {

    @SerializedName("shift_state")
    @Expose
    private Object shiftState;
    @SerializedName("speed")
    @Expose
    private Object speed;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("heading")
    @Expose
    private Integer heading;
    @SerializedName("gps_as_of")
    @Expose
    private Integer gpsAsOf;

    public Object getShiftState() {
        return shiftState;
    }

    public void setShiftState(Object shiftState) {
        this.shiftState = shiftState;
    }

    public Object getSpeed() {
        return speed;
    }

    public void setSpeed(Object speed) {
        this.speed = speed;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getHeading() {
        return heading;
    }

    public void setHeading(Integer heading) {
        this.heading = heading;
    }

    public Integer getGpsAsOf() {
        return gpsAsOf;
    }

    public void setGpsAsOf(Integer gpsAsOf) {
        this.gpsAsOf = gpsAsOf;
    }
}