package com.tahirietrit.tesla.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macb on 16/08/16.
 */
public class Vehicle {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("vehicle_id")
    @Expose
    private Integer vehicleId;
    @SerializedName("vin")
    @Expose
    private String vin;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("option_codes")
    @Expose
    private String optionCodes;
    @SerializedName("color")
    @Expose
    private Object color;
    @SerializedName("tokens")
    @Expose
    private List<String> tokens = new ArrayList<String>();
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("id_s")
    @Expose
    private String idS;
    @SerializedName("remote_start_enabled")
    @Expose
    private Boolean remoteStartEnabled;
    @SerializedName("calendar_enabled")
    @Expose
    private Boolean calendarEnabled;
    @SerializedName("notifications_enabled")
    @Expose
    private Boolean notificationsEnabled;
    @SerializedName("backseat_token")
    @Expose
    private Object backseatToken;
    @SerializedName("backseat_token_updated_at")
    @Expose
    private Object backseatTokenUpdatedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getOptionCodes() {
        return optionCodes;
    }

    public void setOptionCodes(String optionCodes) {
        this.optionCodes = optionCodes;
    }

    public Object getColor() {
        return color;
    }

    public void setColor(Object color) {
        this.color = color;
    }

    public List<String> getTokens() {
        return tokens;
    }

    public void setTokens(List<String> tokens) {
        this.tokens = tokens;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIdS() {
        return idS;
    }

    public void setIdS(String idS) {
        this.idS = idS;
    }

    public Boolean getRemoteStartEnabled() {
        return remoteStartEnabled;
    }

    public void setRemoteStartEnabled(Boolean remoteStartEnabled) {
        this.remoteStartEnabled = remoteStartEnabled;
    }

    public Boolean getCalendarEnabled() {
        return calendarEnabled;
    }

    public void setCalendarEnabled(Boolean calendarEnabled) {
        this.calendarEnabled = calendarEnabled;
    }

    public Boolean getNotificationsEnabled() {
        return notificationsEnabled;
    }

    public void setNotificationsEnabled(Boolean notificationsEnabled) {
        this.notificationsEnabled = notificationsEnabled;
    }

    public Object getBackseatToken() {
        return backseatToken;
    }

    public void setBackseatToken(Object backseatToken) {
        this.backseatToken = backseatToken;
    }

    public Object getBackseatTokenUpdatedAt() {
        return backseatTokenUpdatedAt;
    }

    public void setBackseatTokenUpdatedAt(Object backseatTokenUpdatedAt) {
        this.backseatTokenUpdatedAt = backseatTokenUpdatedAt;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "backseatToken=" + backseatToken +
                ", id=" + id +
                ", vehicleId=" + vehicleId +
                ", vin='" + vin + '\'' +
                ", displayName='" + displayName + '\'' +
                ", optionCodes='" + optionCodes + '\'' +
                ", color=" + color +
                ", tokens=" + tokens +
                ", state='" + state + '\'' +
                ", idS='" + idS + '\'' +
                ", remoteStartEnabled=" + remoteStartEnabled +
                ", calendarEnabled=" + calendarEnabled +
                ", notificationsEnabled=" + notificationsEnabled +
                ", backseatTokenUpdatedAt=" + backseatTokenUpdatedAt +
                '}';
    }
}