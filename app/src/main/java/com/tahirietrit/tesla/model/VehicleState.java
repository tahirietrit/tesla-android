package com.tahirietrit.tesla.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class VehicleState {

    @SerializedName("api_version")
    @Expose
    private Integer apiVersion;
    @SerializedName("autopark_state")
    @Expose
    private String autoparkState;
    @SerializedName("autopark_state_v2")
    @Expose
    private String autoparkStateV2;
    @SerializedName("autopark_style")
    @Expose
    private String autoparkStyle;
    @SerializedName("calendar_supported")
    @Expose
    private Boolean calendarSupported;
    @SerializedName("car_type")
    @Expose
    private String carType;
    @SerializedName("car_version")
    @Expose
    private String carVersion;
    @SerializedName("center_display_state")
    @Expose
    private Integer centerDisplayState;
    @SerializedName("dark_rims")
    @Expose
    private Boolean darkRims;
    @SerializedName("df")
    @Expose
    private Integer df;
    @SerializedName("dr")
    @Expose
    private Integer dr;
    @SerializedName("exterior_color")
    @Expose
    private String exteriorColor;
    @SerializedName("ft")
    @Expose
    private Integer ft;
    @SerializedName("has_spoiler")
    @Expose
    private Boolean hasSpoiler;
    @SerializedName("homelink_nearby")
    @Expose
    private Boolean homelinkNearby;
    @SerializedName("last_autopark_error")
    @Expose
    private String lastAutoparkError;
    @SerializedName("locked")
    @Expose
    private Boolean locked;
    @SerializedName("notifications_supported")
    @Expose
    private Boolean notificationsSupported;
    @SerializedName("odometer")
    @Expose
    private Double odometer;
    @SerializedName("parsed_calendar_supported")
    @Expose
    private Boolean parsedCalendarSupported;
    @SerializedName("perf_config")
    @Expose
    private String perfConfig;
    @SerializedName("pf")
    @Expose
    private Integer pf;
    @SerializedName("pr")
    @Expose
    private Integer pr;
    @SerializedName("rear_seat_heaters")
    @Expose
    private Integer rearSeatHeaters;
    @SerializedName("remote_start")
    @Expose
    private Boolean remoteStart;
    @SerializedName("remote_start_supported")
    @Expose
    private Boolean remoteStartSupported;
    @SerializedName("rhd")
    @Expose
    private Boolean rhd;
    @SerializedName("roof_color")
    @Expose
    private String roofColor;
    @SerializedName("rt")
    @Expose
    private Integer rt;
    @SerializedName("seat_type")
    @Expose
    private Integer seatType;
    @SerializedName("spoiler_type")
    @Expose
    private String spoilerType;
    @SerializedName("sun_roof_installed")
    @Expose
    private Integer sunRoofInstalled;
    @SerializedName("sun_roof_percent_open")
    @Expose
    private Integer sunRoofPercentOpen;
    @SerializedName("sun_roof_state")
    @Expose
    private String sunRoofState;
    @SerializedName("third_row_seats")
    @Expose
    private String thirdRowSeats;
    @SerializedName("valet_mode")
    @Expose
    private Boolean valetMode;
    @SerializedName("vehicle_name")
    @Expose
    private String vehicleName;
    @SerializedName("wheel_type")
    @Expose
    private String wheelType;

    public Integer getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(Integer apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getAutoparkState() {
        return autoparkState;
    }

    public void setAutoparkState(String autoparkState) {
        this.autoparkState = autoparkState;
    }

    public String getAutoparkStateV2() {
        return autoparkStateV2;
    }

    public void setAutoparkStateV2(String autoparkStateV2) {
        this.autoparkStateV2 = autoparkStateV2;
    }

    public String getAutoparkStyle() {
        return autoparkStyle;
    }

    public void setAutoparkStyle(String autoparkStyle) {
        this.autoparkStyle = autoparkStyle;
    }

    public Boolean getCalendarSupported() {
        return calendarSupported;
    }

    public void setCalendarSupported(Boolean calendarSupported) {
        this.calendarSupported = calendarSupported;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getCarVersion() {
        return carVersion;
    }

    public void setCarVersion(String carVersion) {
        this.carVersion = carVersion;
    }

    public Integer getCenterDisplayState() {
        return centerDisplayState;
    }

    public void setCenterDisplayState(Integer centerDisplayState) {
        this.centerDisplayState = centerDisplayState;
    }

    public Boolean getDarkRims() {
        return darkRims;
    }

    public void setDarkRims(Boolean darkRims) {
        this.darkRims = darkRims;
    }

    public Integer getDf() {
        return df;
    }

    public void setDf(Integer df) {
        this.df = df;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getExteriorColor() {
        return exteriorColor;
    }

    public void setExteriorColor(String exteriorColor) {
        this.exteriorColor = exteriorColor;
    }

    public Integer getFt() {
        return ft;
    }

    public void setFt(Integer ft) {
        this.ft = ft;
    }

    public Boolean getHasSpoiler() {
        return hasSpoiler;
    }

    public void setHasSpoiler(Boolean hasSpoiler) {
        this.hasSpoiler = hasSpoiler;
    }

    public Boolean getHomelinkNearby() {
        return homelinkNearby;
    }

    public void setHomelinkNearby(Boolean homelinkNearby) {
        this.homelinkNearby = homelinkNearby;
    }

    public String getLastAutoparkError() {
        return lastAutoparkError;
    }

    public void setLastAutoparkError(String lastAutoparkError) {
        this.lastAutoparkError = lastAutoparkError;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Boolean getNotificationsSupported() {
        return notificationsSupported;
    }

    public void setNotificationsSupported(Boolean notificationsSupported) {
        this.notificationsSupported = notificationsSupported;
    }

    public Double getOdometer() {
        return odometer;
    }

    public void setOdometer(Double odometer) {
        this.odometer = odometer;
    }

    public Boolean getParsedCalendarSupported() {
        return parsedCalendarSupported;
    }

    public void setParsedCalendarSupported(Boolean parsedCalendarSupported) {
        this.parsedCalendarSupported = parsedCalendarSupported;
    }

    public String getPerfConfig() {
        return perfConfig;
    }

    public void setPerfConfig(String perfConfig) {
        this.perfConfig = perfConfig;
    }

    public Integer getPf() {
        return pf;
    }

    public void setPf(Integer pf) {
        this.pf = pf;
    }

    public Integer getPr() {
        return pr;
    }

    public void setPr(Integer pr) {
        this.pr = pr;
    }

    public Integer getRearSeatHeaters() {
        return rearSeatHeaters;
    }

    public void setRearSeatHeaters(Integer rearSeatHeaters) {
        this.rearSeatHeaters = rearSeatHeaters;
    }

    public Boolean getRemoteStart() {
        return remoteStart;
    }

    public void setRemoteStart(Boolean remoteStart) {
        this.remoteStart = remoteStart;
    }

    public Boolean getRemoteStartSupported() {
        return remoteStartSupported;
    }

    public void setRemoteStartSupported(Boolean remoteStartSupported) {
        this.remoteStartSupported = remoteStartSupported;
    }

    public Boolean getRhd() {
        return rhd;
    }

    public void setRhd(Boolean rhd) {
        this.rhd = rhd;
    }

    public String getRoofColor() {
        return roofColor;
    }

    public void setRoofColor(String roofColor) {
        this.roofColor = roofColor;
    }

    public Integer getRt() {
        return rt;
    }

    public void setRt(Integer rt) {
        this.rt = rt;
    }

    public Integer getSeatType() {
        return seatType;
    }

    public void setSeatType(Integer seatType) {
        this.seatType = seatType;
    }

    public String getSpoilerType() {
        return spoilerType;
    }

    public void setSpoilerType(String spoilerType) {
        this.spoilerType = spoilerType;
    }

    public Integer getSunRoofInstalled() {
        return sunRoofInstalled;
    }

    public void setSunRoofInstalled(Integer sunRoofInstalled) {
        this.sunRoofInstalled = sunRoofInstalled;
    }

    public Integer getSunRoofPercentOpen() {
        return sunRoofPercentOpen;
    }

    public void setSunRoofPercentOpen(Integer sunRoofPercentOpen) {
        this.sunRoofPercentOpen = sunRoofPercentOpen;
    }

    public String getSunRoofState() {
        return sunRoofState;
    }

    public void setSunRoofState(String sunRoofState) {
        this.sunRoofState = sunRoofState;
    }

    public String getThirdRowSeats() {
        return thirdRowSeats;
    }

    public void setThirdRowSeats(String thirdRowSeats) {
        this.thirdRowSeats = thirdRowSeats;
    }

    public Boolean getValetMode() {
        return valetMode;
    }

    public void setValetMode(Boolean valetMode) {
        this.valetMode = valetMode;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getWheelType() {
        return wheelType;
    }

    public void setWheelType(String wheelType) {
        this.wheelType = wheelType;
    }

}