package com.tahirietrit.tesla.api;

import com.tahirietrit.tesla.model.Token;
import com.tahirietrit.tesla.resonse.ChargingStatusResponse;
import com.tahirietrit.tesla.resonse.ClimateStateResponse;
import com.tahirietrit.tesla.resonse.DrivePositionResponse;
import com.tahirietrit.tesla.resonse.GuiSettingsResponse;
import com.tahirietrit.tesla.resonse.MobileEnabledResponse;
import com.tahirietrit.tesla.resonse.SetChargingStatusResponse;
import com.tahirietrit.tesla.resonse.VehicleStateResponse;
import com.tahirietrit.tesla.resonse.VehiclesResponse;

import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import rx.Observable;


public interface ApiService {
    @FormUrlEncoded
    @POST("/oauth/token")
    Observable<Token> getToken(@Field("grant_type") String grantType,
                               @Field("client_id") String clientId,
                               @Field("client_secret") String clientSecret,
                               @Field("email") String email,
                               @Field("password") String password);
    @Multipart
    @POST("Account/checkEmail")
    android.database.Observable<Boolean> checkEmail(@Part("email") String email, @Part("action") String gjirafaKey);
    @GET("/api/1/vehicles")
    Observable<Response<VehiclesResponse>> getVehicles(@Header("Authorization") String token);

    @GET("/api/1/vehicles/{id}/data_request/charge_state")
    Observable<Response<ChargingStatusResponse>> getChargingStatus(@Header("Authorization") String token,
                                                                   @Path("id") long id);

    @FormUrlEncoded
    @POST("/api/1/vehicles/{id}/command/set_charge_limit")
    Observable<Response<SetChargingStatusResponse>> setChargingLimit(@Header("Authorization") String token,
                                                                     @Path("id") long id,
                                                                     @Field("percent") int limit_value);

    @GET("/api/1/vehicles/{id}/data_request/climate_state")
    Observable<Response<ClimateStateResponse>> getClimateState(@Header("Authorization") String token,
                                                               @Path("id") long id);

    @POST("/api/1/vehicles/{id}/command/auto_conditioning_start")
    Observable<Response<SetChargingStatusResponse>> startAC(@Header("Authorization") String token,
                                                            @Path("id") long id);

    @POST("/api/1/vehicles/{id}/command/auto_conditioning_stop")
    Observable<Response<SetChargingStatusResponse>> stopAC(@Header("Authorization") String token,
                                                           @Path("id") long id);


    @GET("/api/1/vehicles/{id}/data_request/vehicle_state")
    Observable<Response<VehicleStateResponse>> getVehicleState(@Header("Authorization") String token,
                                                               @Path("id") long id);

    @FormUrlEncoded
    @POST("/api/1/vehicles/{id}/command/sun_roof_control")
    Observable<Response<SetChargingStatusResponse>> controlTheRoof(@Header("Authorization") String token,
                                                                   @Path("id") long id,
                                                                   @Field("state") String vent);

    @GET("/api/1/vehicles/{id}/mobile_enabled")
    Observable<Response<MobileEnabledResponse>> getMobileEnabled(@Header("Authorization") String token,
                                                                 @Path("id") long id);

    @GET("/api/1/vehicles/{id}/data_request/drive_state")
    Observable<Response<DrivePositionResponse>> getDrivingPosition(@Header("Authorization") String token,
                                                                   @Path("id") long id);

    @GET("/api/1/vehicles/{id}/data_request/gui_settings")
    Observable<Response<GuiSettingsResponse>> getGuiSettings(@Header("Authorization") String token,
                                                             @Path("id") long id);


}
