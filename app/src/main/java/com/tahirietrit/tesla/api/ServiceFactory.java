package com.tahirietrit.tesla.api;

import com.tahirietrit.tesla.eventbus.RxBus;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by tahirietrit on 12/08/16.
 */
public class ServiceFactory {
    public static final String BASE_URL = "https://owner-api.teslamotors.com";

    public static <T> T createRetrofitService(final Class<T> clazz, final String endPoint, final RxBus rxBus) {

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request request = chain.request();
                    rxBus.send(request);
                    return chain.proceed(chain.request());
                })
                .build();

        final Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(endPoint)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        T service = restAdapter.create(clazz);

        return service;
    }
    public static <T> T createRetrofitService(final Class<T> clazz, final String endPoint) {

        final Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(endPoint)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        T service = restAdapter.create(clazz);

        return service;
    }
}
