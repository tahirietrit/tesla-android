package com.tahirietrit.tesla.utils;

import android.support.annotation.Nullable;
import android.widget.TextView;

import retrofit2.Response;

public class LoggingView {

    @Nullable
    private TextView logingTextView;

    public void addNewReponseToLog(String reponse) {
        if (logingTextView != null) {
            String currentLog = logingTextView.getText().toString();
            logingTextView.setText(reponse + "\n\n" + currentLog);
        }
    }

    public void addNewRequestdataToLog(String request) {
        if (logingTextView != null) {
            String currentLog = logingTextView.getText().toString();
            logingTextView.setText(request + "\n\n" + currentLog);
        }
    }

    public void addExcutionTimeToLog(Response response) {
        if (logingTextView != null) {
            long executiontime = (response.raw().receivedResponseAtMillis()
                    - response.raw().sentRequestAtMillis());
            float executionTime = executiontime / 1000f;
            String currentLog = logingTextView.getText().toString();
            logingTextView.setText(String.format("%s%s%s", "Request took: ", executionTime, "s") + "\n\n" + currentLog);
        }
    }

    public String getLog() {
        if (logingTextView != null) {
            return logingTextView.getText().toString();
        } else {
            throw new IllegalStateException("Logging View is not initialized.\nMake sure to setLogingTextView() before trying to get log.");
        }
    }

    public void setLogingTextView(TextView textView) {
        logingTextView = textView;
    }
}
