package com.tahirietrit.tesla.utils;


public class ConvertUtilitys {
    private static final double MILES_TO_KM = 1.60934;

    public static long voidConvertMiToKm(Double miles) {
        return Math.round(miles * MILES_TO_KM);
    }

    public static String getRemeaningTime(Double timeRemeaning) {
        int hours = timeRemeaning.intValue();
        int minutes = (int) ((timeRemeaning - timeRemeaning.intValue()) * 60);
        return String.format("%d hr %02d min", hours, minutes);
    }

}
