package com.tahirietrit.tesla.di;

import com.tahirietrit.tesla.ui.LoginActivity;
import com.tahirietrit.tesla.ui.MainActivity;
import com.tahirietrit.tesla.ui.fragments.ApiExploreFragment;
import com.tahirietrit.tesla.ui.fragments.ChargingStatusFragment;
import com.tahirietrit.tesla.ui.fragments.RoofAcFragment;
import com.tahirietrit.tesla.widget.aircondition.AirConditionWidgetService;
import com.tahirietrit.tesla.widget.chargingstate.ChargingStatusWidgetService;
import com.tahirietrit.tesla.widget.combinedwidget.CombinedWidgetService;
import com.tahirietrit.tesla.widget.roofventwidget.RoofVentWidgetService;

import javax.inject.Singleton;

import dagger.Component;
@Singleton
@Component(modules = {DataModule.class, AppModule.class, ApiModule.class})
public interface TeslaComponent {

    void inject(LoginActivity activity);
    void inject(MainActivity activity);
    void inject(ChargingStatusWidgetService service);
    void inject(AirConditionWidgetService service);
    void inject(RoofVentWidgetService service);
    void inject(CombinedWidgetService service);
    void inject(ChargingStatusFragment fragment);
    void inject(RoofAcFragment fragment);
    void inject(ApiExploreFragment fragment);
}
