package com.tahirietrit.tesla.di;

import com.tahirietrit.tesla.api.ApiService;
import com.tahirietrit.tesla.api.ServiceFactory;
import com.tahirietrit.tesla.eventbus.RxBus;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class ApiModule {
    @Provides
    @Singleton
    @Named("withRxBus")
    ApiService provideApiService(RxBus rxBus) {
        return ServiceFactory.createRetrofitService(ApiService.class, ServiceFactory.BASE_URL, rxBus);
    }

    @Provides
    @Singleton
    @Named("withoutRxBus")
    ApiService provideApiServiceWithoutBus() {
        return ServiceFactory.createRetrofitService(ApiService.class, ServiceFactory.BASE_URL);
    }

}