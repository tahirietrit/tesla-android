package com.tahirietrit.tesla.di;

import android.content.Context;

import com.tahirietrit.tesla.eventbus.RxBus;
import com.tahirietrit.tesla.persistency.AppPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tahirietrit on 15/08/16.
 */
@Module
public final class DataModule {

    @Provides
    @Singleton
    AppPreferences provideSharedPreferences(Context ctx) {
        return new AppPreferences(ctx);
    }

    @Provides
    @Singleton
    RxBus provideRxBus() {
        return new RxBus();
    }
}
