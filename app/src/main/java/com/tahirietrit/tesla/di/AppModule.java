package com.tahirietrit.tesla.di;

import android.content.Context;

import com.tahirietrit.tesla.TeslaApplication;
import com.tahirietrit.tesla.utils.LoggingView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by macb on 15/08/16.
 */
@Module
public class AppModule {

    private final TeslaApplication app;

    public AppModule(TeslaApplication app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return app;
    }

    @Provides
    @Singleton
    public LoggingView provideLoggingView() {
        return new LoggingView();
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        return new GsonBuilder().setPrettyPrinting().create();
    }
}
