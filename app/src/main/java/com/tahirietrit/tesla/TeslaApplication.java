package com.tahirietrit.tesla;

import android.app.Application;

import com.tahirietrit.tesla.di.ApiModule;
import com.tahirietrit.tesla.di.AppModule;
import com.tahirietrit.tesla.di.DaggerTeslaComponent;
import com.tahirietrit.tesla.di.DataModule;
import com.tahirietrit.tesla.di.TeslaComponent;

/**
 * Created by macb on 15/08/16.
 */
public class TeslaApplication extends Application {

    private static TeslaComponent teslaComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        if(teslaComponent == null) {
            teslaComponent = DaggerTeslaComponent.builder()
                    .appModule(new AppModule(this))
                    .dataModule(new DataModule())
                    .apiModule(new ApiModule())
                    .build();
        }
    }

    public TeslaComponent getComponent() {
        return teslaComponent;
    }

}
